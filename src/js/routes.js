"use strict";

/*the pages*/
var template = {
    /*an abstract view that allows the content to share the same wrapper*/
    frame: { 
        name: "template",
        abstract: true,
        views: {
            template: {
                templateUrl: "/pages/template/template.html"
            }
        }
    },
    /*the login page*/
    login: {
        name: "template.login",
        parent: "template",
        url: "/login",
        views: {
            content: {
                templateUrl: "/pages/login/login.html",
                controller: "loginCtrl"
            }
        }
    },
    /*the sample dashboard*/
    dashboard: {
        name: "template.dashboard",
        parent: "template",
        url: "/dashboard",
        views: {
            content: {
                templateUrl: "/pages/dashboard/dashboard.html",
                controller: "dashboardCtrl"
            }
        }
    },
    /*the sample table*/
    table: {
        name: "template.tables",
        parent: "template",
        url: "/tables",
        parent: "template",
        views: {
            content: {
                templateUrl: "/pages/tables/tables.html",
                controller: "tableCtrl"
            }
        }
    },
    /*the sample table*/
    networkAll: {
        name: "template.networkAll",
        parent: "template",
        url: "/network-all",
        parent: "template",
        views: {
            content: {
                templateUrl: "/pages/network-all/network-all.html",
                controller: "networkAllCtrl"
            }
        }
    }
}

angular.module("KICT").config(["$stateProvider", "$urlRouterProvider",
    function ($stateProvider, $urlRouterProvider) {
        //for unmatched routes
        $urlRouterProvider.otherwise("/login");

        //for matched routes
        angular.forEach(template, function(value, key){
            $stateProvider.state(value);
        });
    }
]);