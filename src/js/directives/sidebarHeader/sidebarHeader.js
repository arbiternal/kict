var sidebarHeader = function () {
    var directive = {
        restrict: "AE",
        templateUrl: "directives/sidebarHeader/sidebarHeader.html"
    };
    return directive;
};

angular
    .module("KICT")
    .directive("sidebarHeader", sidebarHeader);