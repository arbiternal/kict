var sidebar = function () {
    var directive = {
        restrict: "AE",
        transclude: true,
        templateUrl: "directives/sidebar/sidebar.html"
    };
    return directive;
};

angular
    .module("KICT")
    .directive("sidebar", sidebar);