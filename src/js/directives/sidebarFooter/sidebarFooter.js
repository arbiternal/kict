var sidebarFooter = function () {
    var directive = {
        restrict: "AE",
        templateUrl: "directives/sidebarFooter/sidebarFooter.html"
    };
    return directive;
};

angular
    .module("KICT")
    .directive("sidebarFooter", sidebarFooter);