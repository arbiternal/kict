var sidebarContentFunction = function(scope, element, attributes){
    scope.title = attributes.title;
    scope.titleIcon = attributes.titleIcon ? "fa " + attributes.titleIcon : "";
    scope.link = attributes.link;
    scope.open = false;
    
    scope.titleClicked = function ($event){
        return scope.link ? (window.location.href = "#/" + scope.link) : (scope.open = scope.open ? false : true);
    };
    
    return scope;
};

var sidebarContent = function () {
    var directive = {
        restrict: "AE",
        transclude: true,
        templateUrl: "directives/sidebarContent/sidebarContent.html",
        replace: true,
        scope: true,
        link: sidebarContentFunction
    };
    return directive;
};

angular
    .module("KICT")
    .directive("sidebarContent", sidebarContent);