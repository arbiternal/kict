/**
 * commonly shared service
 */

var commonService = function () {
    var service = this;

    /*string prototypes*/
    service.stringPrototypes = function () {
        /*add capitalization functions to the String object*/
        String.prototype.capitalize = function () {
            return this.charAt(0).toUpperCase() + this.slice(1);
        };

        return console.log("initialized String prototypes");
    };

    service.prototype = function () {
        service.stringPrototypes();

        return console.log("initialized prototypes");
    };

    service.init = function () {
        service.prototype();

        return console.log("initialized common services")
    };

    return service;
};

angular.module("KICT").service("common", [commonService]);