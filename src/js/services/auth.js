/**
 * User Auth checking service
 */

var authService = function ($cookies, $state) {
    /*set a service*/
    var service = this;
    /*session activity is true for testing, false for deployment*/
    service.session = true;
    /*check for all local preferences*/
    service.checkLocalPreferences = function () {
        /*assign a default preference*/
        var defaultPreference = {
            remember: true
        };
        /*attempt to get local preferences, if none exist, overwrite*/
        return service.getUser() || defaultPreference;
    };
    /*fetch the saved user info*/
    service.getUser = function () {
        var defaultUserInfo = {
            session: false
        };
        return $cookies.getObject("user") || defaultUserInfo;
    };
    /*check if the user is valid (NYI)*/
    service.checkUser = function (user) {
        return user.name && user.password;
    };
    /*save a user defined user object*/
    service.setUser = function (user) {
        return $cookies.putObject("user", user);
    };
    /*log in*/
    service.login = function (user) {
        if (!user) user = service.getUser();
        /*if the user validation fails, return an error message*/
        if (!service.checkUser(user)) return alert("Failed to log in");
        /*turn on session*/
        service.session = true;
        /*if the login is successful, save the user data and move to a local page*/
        service.setUser(user);
        /*reload page if it's not login page, otherwise move to dashboards*/
        var targetLocation = $state.current.name == "template.login" ? "template.networkAll" : $state.current.name;
        var reloadOption = {
            reload: true
        };
        return $state.go(targetLocation, {}, reloadOption);
    };
    /*log out*/
    service.logout = function () {
        /*turn off session*/
        service.session = false;
        /*go to login page*/
        var reloadOption = {
            reload: true
        };
        $state.go("template.login", {}, reloadOption);
    };
    /*check if the user session is valid*/
    service.checkUser = function () {
        var currentpage = {};
        if ($state.current.name !== 'template.login' && !service.session) {
            service.logout();
        } else {
            /*the currentpage.title should be a switch case*/
            currentpage.title = $state.current.name.slice(9);
            currentpage.breadcrumbs = "";
        }
        return currentpage;
    };
    /*get toggle settings*/
    service.getToggle = function () {
        return $cookies.get("toggle") ? true : false;
    };
    /*set toggle settings*/
    service.setToggle = function (toggle) {
        return $cookies.put("toggle", toggle);
    };
    /*return the service object*/
    return service;
};

angular.module("KICT").service("auth", ["$cookies", "$state", authService]);