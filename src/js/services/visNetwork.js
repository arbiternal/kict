/**
 * vis.js network chart service
 */

var visNetwork = function ($rootScope, $http) {
    var service = this;

    service.target = "network-chart";
    service.dataSource = "./files/data.json";

    service.data = {
        nodes: new vis.DataSet([]),
        edges: new vis.DataSet([])
    };

    /*default colors*/
    service.colors = {
        technicFirstNode: "#ff6655",
        technicFirstLink: "#ff6655",
        technicSecondNode: "#ffafa6",
        technicSecondLink: "#ffafa6",
        technicThirdNode: "#ffe3e0",
        technicThirdLink: "#ffe3e0",
        employeeNode: "#6485f3",
        employeeLink: "#6485f3",
        employeeText: "#ffffff",
        locationNode: "#aedc84",
        locationLink: "#99bc22"
    };

    service.groups = {
        technicFirst: {
            shape: "dot",
            color: service.colors.technicFirstNode,
            size: 120,
            font: {
                size: 50
            },
            //mass: 5
        },
        technicSecond: {
            shape: "dot",
            color: service.colors.technicSecondNode,
            size: 70,
            font: {
                size: 30
            },
            //mass: 3
        },
        technicThird: {
            shape: "dot",
            color: service.colors.technicThirdNode,
            size: 50,
            font: {
                size: 20
            },
            //mass: 3
        },
        employee: {
            shape: "circle",
            color: service.colors.employeeNode,
            font: {
                size: 20,
                color: service.colors.employeeText
            },
            //mass: 3
        },
        location: {
            shape: "box",
            color: service.colors.locationNode,
            size: 60,
            font: {
                size: 180
            },
            margin: 30,
            //mass: 7
        }
    };

    service.options = {
        groups: service.groups,
        edges: {
            smooth: false
        },
        physics: {
            stabilization: {
                enabled: true,
                iterations: 150
            },
            barnesHut: {
                gravitationalConstant: -80000,
                springConstant: 0.001,
                springLength: 20
            }
        },
        layout: {
            improvedLayout: false
        }
    };

    /*fetches and organizes the data from the data source*/
    service.getData = function (dataSource) {
        /*if the data source is not defined, use the one in the service*/
        dataSource = dataSource || service.dataSource;

        return $http.get(dataSource).then(function (res) {
            /*saving raw data for reference*/
            service.jsonData = res.data;
            /*these will be sorted node data*/
            service.employees = [];
            service.techniques = [];
            /*these will be sorted link data*/
            service.employeeLinks = [];
            service.techniqueLinks = [];
            /*service is the sorted raw technic data*/
            service.technic = {
                first: {},
                second: {},
                third: {}
            };

            /*sort through the employee data*/
            angular.forEach(res.data, function (employeeValue, employeeKey) {
                /*making an employee object*/
                var employee = {
                    _id: employeeValue._id,
                    id: employeeValue.username, //using username as node id
                    administrator: employeeValue.administrator,
                    label: employeeValue.name, //using name as label
                    patent: employeeValue.patent,
                    thesis: employeeValue.thesis,
                    fund: employeeValue.fund,
                    phone: employeeValue.phone,
                    email: employeeValue.email,
                    position: employeeValue.position,
                    department: employeeValue.department,
                    technic: []
                };

                /*sort through the technics assigned to each employee*/
                angular.forEach(employeeValue.technics, function (technicValue, technicKey) {
                    /*대*/
                    service.technic.first[technicValue.first.cdBdn] = technicValue.first;
                    /*중*/
                    service.technic.second[technicValue.second.cdBdn] = technicValue.second;
                    /*소*/
                    service.technic.third[technicValue.third.cdBdn] = technicValue.third;
                    /*detail은 소 중에도 불류 되지만 쉽게 하기 위해 그냥 한다*/
                    service.technic.third[technicValue.third.cdBdn]["detail"] = technicValue.detail.cdBdnNm;
                    /*making a technic object*/
                    var technic = {
                        first: technicValue.first.cdBdn,
                        second: technicValue.second.cdBdn,
                        third: technicValue.third.cdBdn
                    };
                    /*assign the technic to the employee*/
                    employee.technic.push(technic);
                });
                /*push the employee into the list of employees*/
                service.employees.push(employee);
            });

            /*return the raw data*/
            return service.jsonData;
        });
    };

    /*generates a dataset for the medium pool*/
    service.generateSkillData = function (apply, dataSetName) {
        var dataSetNameCap = dataSetName.capitalize();
        /*creates the skill node and link arrays*/
        service["technic" + dataSetNameCap] = [];
        service["technic" + dataSetNameCap + "Links"] = [];

        angular.forEach(service.technic[dataSetName], function (technicVal, technicKey) {
            var technique = {
                id: technicVal.cdBdn,
                label: technicVal.cdBdnNm,
                group: "technic" + dataSetNameCap
            };

            service["technic" + dataSetNameCap].push(technique);

            if (technicVal.uppCdBdn) {
                var technicLink = {
                    from: technicVal.cdBdn,
                    to: technicVal.uppCdBdn,
                    color: service.colors["technic" + dataSetNameCap + "Link"],
                    width: 10
                };

                service["technic" + dataSetNameCap + "Links"].push(technicLink);
            }
        });

        if (apply) {
            service.data.nodes.add(service["technic" + dataSetNameCap]);
            service.data.edges.add(service["technic" + dataSetNameCap + "Links"]);
        }
    };

    /*links employees to techniques*/
    service.generateEmployeeData = function (apply) {
        angular.forEach(service.employees, function (employeeVal, employeeKey) {
            angular.forEach(employeeVal.technic, function (technicVal, technicKey) {
                var firstLink = {
                    from: employeeVal.id,
                    to: technicVal.first,
                    type: "first"
                };
                service.employeeLinks.push(firstLink);
                var secondLink = {
                    from: employeeVal.id,
                    to: technicVal.second,
                    type: "second"
                };
                service.employeeLinks.push(secondLink);
                var thirdLink = {
                    from: employeeVal.id,
                    to: technicVal.third,
                    type: "third"
                };
                service.employeeLinks.push(thirdLink);
            });

            employeeVal["group"] = "employee"
        });

        if (apply) {
            service.data.nodes.add(service.employees);
            service.data.edges.add(service.employeeLinks);
        }
    };

    /*links employees to departments*/
    service.generateDepartmentData = function (apply) {
        service.deptNodes = [];
        service.deptNodeList = {};
        service.deptLinks = [];

        angular.forEach(service.employees, function (employeeVal, employeeKey) {
            var deptNode = {
                id: employeeVal.department.code,
                label: employeeVal.department.name,
                group: "location"
            }
            service.deptNodeList[employeeVal.department.code] = deptNode;
            var deptLink = {
                from: employeeVal.id,
                to: employeeVal.department.code,
                width: 20,
                color: service.colors.locationLink
            };

            angular.forEach(employeeVal.technic, function (technicVal, technicKey) {
                var deptFirstLink = {
                    from: technicVal.first,
                    to: employeeVal.department.code,
                    width: 20,
                    color: service.colors.locationLink
                };
                service.deptLinks.push(deptFirstLink);
                var deptSecondLink = {
                    from: technicVal.second,
                    to: employeeVal.department.code,
                    width: 15,
                    color: service.colors.locationLink
                };
                service.deptLinks.push(deptSecondLink);
                var deptThirdLink = {
                    from: technicVal.third,
                    to: employeeVal.department.code,
                    width: 10,
                    color: service.colors.locationLink
                };
                service.deptLinks.push(deptThirdLink);
            });

            return service.deptLinks.push(deptLink);
        });

        angular.forEach(service.deptNodeList, function (deptVal, deptKey) {
            return service.deptNodes.push(deptVal);
        });

        if (apply) {
            service.data.nodes.add(service.deptNodes);
            service.data.edges.add(service.deptLinks);
        }
    };

    var expandNode = function (focalNode) {
        focalNode = focalNode instanceof String ? service.data.nodes.get(focalNode) : focalNode;
        service.startData.edges.forEach(function (edgesVal, edgesKey) {
            var nodeToAdd;
            if (edgesVal.from == focalNode.id) {
                nodeToAdd = edgesVal.to;
            } else if (edgesVal.to == focalNode.id) {
                nodeToAdd = edgesVal.from;
            }

            if (nodeToAdd && !service.data.nodes.get(nodeToAdd)) {
                service.data.nodes.add(service.startData.nodes.get(nodeToAdd));
            }
        });
    };

    var grabFocusNodes = function (focalNode, keepFocus) {
        focalNode = focalNode instanceof String ? service.data.nodes.get(focalNode) : focalNode;

        var nodes = keepFocus ? [focalNode] : [];

        var newData = {
            nodes: new vis.DataSet(nodes),
            edges: service.data.edges
        };

        if (focalNode.group == "technicThird") {
            var second = service.startData.edges.get({
                filter: function (item) {
                    return (item.from == focalNode.id) && (item.from.indexOf(item.to) > -1);
                }
            })[0];
            var first = service.startData.edges.get({
                filter: function (item) {
                    return (item.from == second.to) && (item.from.indexOf(item.to) > -1);
                }
            })[0];

            newData.nodes.add(service.startData.nodes.get(first.to));
        }

        service.data.edges.forEach(function (edgesVal, edgesKey) {
            var nodeToAdd;
            if (edgesVal.from == focalNode.id) {
                nodeToAdd = edgesVal.to;
            } else if (edgesVal.to == focalNode.id) {
                nodeToAdd = edgesVal.from;
            }

            if (nodeToAdd && !newData.nodes.get(nodeToAdd)) {
                newData.nodes.add(service.startData.nodes.get(nodeToAdd));
            }
        });

        return newData;
    };

    var drawLocation = function (focalNode, keepFocus) {
        grabFocusNodes(focalNode, keepFocus);

        service.data = newData;
        /*spread the options a bit further*/
        var options = angular.copy(service.options);
        options.nodes = {
            mass: 8
        };

        return service.draw(service.target, newData, options);
    };

    var drawLocations = function (focalNodes, keepFocus) {
        var newData = {
            nodes: new vis.DataSet([]),
            edges: service.startData.edges
        };
        angular.forEach(focalNodes, function (focalVal, focalKey) {
            var focus = service.startData.nodes.get(focalVal);
            var nodesToAdd = grabFocusNodes(focus, keepFocus).nodes.get();
            angular.forEach(nodesToAdd, function (addNode, addNodeKey) {
                if (!newData.nodes.get(addNode.id)) newData.nodes.add(addNode);
            });
        });

        service.data = newData;

        return service.draw(service.target, newData, service.options);
    };

    service.drawLocation = function (deptId, keepFocus) {
        keepFocus = keepFocus || false;
        return deptId.constructor == Array ? drawLocations(deptId, keepFocus) : drawLocation(service.startData.nodes.get(deptId), keepFocus);
    };

    service.onAfterDraw = function () {
        service.networkChart.on("click", function (event) {
            var focalNode = service.data.nodes.get(event.nodes[0]);
            if (!focalNode.id) {
                return false;
            } else {
                return expandNode(focalNode);
            }
        });

        service.networkChart.on("doubleClick", function (event) {
            var focalNode = service.data.nodes.get(event.nodes[0]);
            if (!focalNode.id) {
                return false;
            }
            if (focalNode.group == "location") {
                return drawLocation(focalNode, false);
            } else {
                return drawLocation(focalNode, true);
            }
        });
        
        service.networkChart.on("zoom", function (event){
            var maxScale = 0.04;
            var scaleTarget = {
                scale: maxScale
            };
            return event.scale < maxScale ? service.networkChart.moveTo(scaleTarget) : event;
        });
    };

    /*drawing function*/
    service.draw = function (target, data, options) {
        target = document.getElementById(target || service.target);
        data = data || service.startData;
        options = options || service.options;
        
        service.data = data;

        options.physics.stabilization.iterations = data.nodes.length * 2 > 500 ? 500 : data.nodes.length * 2;
        //options.physics.stabilization.iterations = 0;

        if (service.networkChart) service.networkChart.destroy();

        service.networkChart = new vis.Network(target, data, options);
        service.onAfterDraw();

        $rootScope.$broadcast("visNetworkDrawn");

        return service.networkChart;
    };

    service.findNode = function (nodeName) {
        var nodeToGet = service.data.nodes.get({
            filter: function (item) {
                return !nodeName ? false : item.label.indexOf(nodeName) > -1 || item.id == nodeName;
            }
        });

        var otherNodes = service.data.nodes.get({
            filter: function (item) {
                return !nodeName ? true : item.label.indexOf(nodeName) == -1 && item.id != nodeName;
            }
        });
        
        //convert to array if not an array
        if (!(nodeToGet instanceof Array)) nodeToGet = [nodeToGet];

        angular.forEach(otherNodes, function (nodeVal, nodeKey) {
            delete nodeVal.size;
            delete nodeVal.font;
            delete nodeVal.color;

            return nodeVal;
        });

        angular.forEach(nodeToGet, function (nodeVal, nodeKey) {
            nodeVal.size = 150;
            nodeVal.font = {
                size: 50,
                color: "#0000FF"
            };
            nodeVal.color = "#FFFF00";
            return nodeVal;
        });

        service.data.nodes.update(otherNodes);
        service.data.nodes.update(nodeToGet);

        service.startData.nodes.update(otherNodes);
        service.startData.nodes.update(nodeToGet);

        return nodeToGet;
    };

    service.$on = function () {
        return $rootScope.$on(arguments[0], arguments[1], arguments[3]);
    };

    /*initialization function*/
    service.init = function (target, data) {
        /*use user stated data*/
        service.target = target || service.target;
        service.dataSource = data || service.dataSource;

        /*reset data*/
        service.data = {
            nodes: new vis.DataSet([]),
            edges: new vis.DataSet([])
        };

        /*fetch data*/
        return service.getData().then(function () {
            service.generateEmployeeData(true);
            service.generateSkillData(true, "first");
            service.generateSkillData(true, "second");
            service.generateSkillData(true, "third");
            service.generateDepartmentData(true);

            service.startData = angular.copy(service.data);

            $rootScope.$broadcast("visNetworkInit");

            return service.draw(service.target,service.data,service.options);
        });
    };

    /*return the service object*/
    return service;
};

angular.module("KICT").service("visNetwork", ["$rootScope", "$http", visNetwork]);