/**
 * dashboard Controller
 */

var networkAllCtrl = function ($scope, visNetwork) {
    $scope.initVisNetwork = function(){
        return $scope.networkChart = visNetwork.init("network-all-container", "./files/data.json");
    };
    
    $scope.init = function(){
        return $scope.initVisNetwork();
    };
    
    return $scope.init();
};

angular.module("KICT").controller("networkAllCtrl", ["$scope", "visNetwork", networkAllCtrl]);