/**
 * Master Controller
 */

var templateCtrl = function ($scope, $cookies, auth, common, visNetwork/*, $mdSidenav*/) {
//
//    $scope.toggleNavigation = function () {
//        $mdSidenav('navigation-drawer').toggle();
//    };


    /*applies things such as string prototyping*/
    common.init();

    $scope.logout = auth.logout;

    $scope.locationLabel = "전체 네트워크";

    $scope.init = function () {
        $scope.currentpage = auth.checkUser();
        $scope.user = auth.getUser();
        $scope.user.session = auth.session;
        $scope.user.department = "건실성책연구소";
        $scope.user.surname = "홍";
        $scope.user.forename = "길동";
        $scope.user.name = $scope.user.surname + $scope.user.forename;
        $scope.user.position = "연구원";
        return $scope.user;
    };

    visNetwork.$on("visNetworkInit", function () {
        var starterObj = {
            ALL: {
                id: "ALL",
                label: "전체 네트워크"
            }
        };
        $scope.locations = angular.merge(starterObj, visNetwork.deptNodeList);

        $scope.deptList = {
            1: {}
        };
        return $scope.locations;
    });

    $scope.$watch("findThis", function (value, prev) {
        return (value || prev) ? visNetwork.findNode(value) : false;
    });

    $scope.addDept = function () {
        var keyList = Object.keys($scope.deptList);
        var largestKey = parseInt(keyList[keyList.length - 1]);
        return $scope.deptList[largestKey + 1] = {};
    };

    $scope.removeDept = function (key) {
        return delete $scope.deptList[key];
    };

    var changedInWatch = false;

    $scope.$watch("deptList", function (value, prev) {
        if (changedInWatch) return changedInWatch = false;

        var listOfThings = [];
        angular.forEach(value, function (dv, dk) {
            if (!dv.location && $scope.locations) {
                dv.location = $scope.locations["ALL"];
                changedInWatch = true;
            } else if (dv.location.id != "ALL") {
                listOfThings.push(dv.location.id);
            }
            return dv;
        });

        $scope.locationLabel = listOfThings.length == 0 ? "전체 네트워크" : listOfThings.length == 1 ? visNetwork.deptNodeList[listOfThings[0]].label : "여러 네트워크";

        return (changedInWatch || !prev) ? false : listOfThings.length == 0 ? visNetwork.draw() : listOfThings.length == 1 ? visNetwork.drawLocation(listOfThings) : visNetwork.drawLocation(listOfThings, true);
    }, true);

    return $scope.init();
};

app.controller("templateCtrl", ["$scope", "$cookies", "auth", "common", "visNetwork"/*, "$mdSidenav"*/, templateCtrl]);