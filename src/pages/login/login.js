/**
 * login Controller
 */

var loginCtrl = function ($scope, auth) {
    /*login event handler*/
    $scope.login = function ($event) {
        /*use the auth service's login*/
        return auth.login($scope.user);
    };

    /*check the local settings to see if the user has previously saved sessions/preferences*/
    $scope.checkLocalPreferences = function () {
        /*check if a session exists and if a session exists, log in*/
        if (auth.session) return auth.login();
        
        /*check local preferences*/
        $scope.user = auth.checkLocalPreferences();
        /*if the preferences don't want the computer to remember the user, hide the data*/
        if (!$scope.user.remember){
            $scope.user.name = "";
            $scope.user.password = "";
        }
        /*return the user info*/
        return $scope.user;
    };

    /*controller init functions*/
    $scope.init = function () {
        return $scope.checkLocalPreferences();
    };

    /*initialize the controller*/
    return $scope.init();
};

app.controller("loginCtrl", ["$scope", "auth", loginCtrl]);